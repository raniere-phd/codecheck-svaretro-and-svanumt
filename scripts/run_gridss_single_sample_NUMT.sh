#!/bin/bash
#SBATCH --job-name=gridss_chr1_numt_pe_HS25
#SBATCH --time=40:00:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=dong.rn@wehi.edu.au
#SBATCH --mem=200G
#SBATCH --cpus-per-task=10
#SBATCH --array=1-1
#SBATCH --ntasks-per-node=8

HOME=/home/users/allstaff/dong.rn
base_dir=$HOME/StructuralVariantAnnotation/bamsurgeon/paper/simReads
ref=$HOME/StructuralVariantAnnotation/bamsurgeon/hs37d5.fa
working_dir=$base_dir/gridss_numt
gridss_dir=$HOME/tools/gridss

###### NUMT sample #########
inputs="$base_dir/chr1_numt_pe_HS25_sorted.bam"
sample_name=chr1_numt_pe_HS25
###### NUMT sample #########

######   RT sample #########
# inputs="$base_dir/chr1_rt_pe_HS25_sorted.bam"
# sample_name=chr1_rt_pe_HS25
######   RT sample #########

inputarr=($inputs)
job_prefix=gridss_${sample_name}

module add bwa samtools R java
mkdir -p $working_dir
cat > $working_dir/gridss.properties << EOF
# The default chunk size of 10Mb only gives ~300 units of work
# reducing to 1Mb gives ~3000 which gives a more even distribution
# of work across jobs
chunkSize=1000000
chunkSequenceChangePenalty=100000
EOF
# cd $base_dir/gridss

$gridss_dir/gridss 	\
    -r $ref \
    -o $base_dir/$sample_name.sv.vcf \
    -a $base_dir/$sample_name.asm.bam \
    -j $gridss_dir/gridss-2.12.0-gridss-jar-with-dependencies.jar \
    -w $working_dir \
    -c $working_dir/gridss.properties \
    -t 8 $inputs
    # -b $base_dir/HG002_SVs_Tier1_v0.6_blacklist.bed $inputs
