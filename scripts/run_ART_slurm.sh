#!/bin/sh

# ---------------resource specifications----------------

#SBATCH --job-name=ART_ALIGNMENT

#SBATCH --time=40:00:00
#SBATCH --mail-type=ALL

#SBATCH --mail-user=dong.rn@wehi.edu.au

#SBATCH --mem=200G
#SBATCH --cpus-per-task=10
# ---------------end of specifications------------------

HOME=/home/users/allstaff/dong.rn
ART=$HOME/StructuralVariantAnnotation/bamsurgeon/paper/art_bin_MountRainier
REF=$HOME/StructuralVariantAnnotation/bamsurgeon/hs37d5.fa
# BAM=$HOME/Papenfuss_lab/projects/sv_benchmark_old/data.HG002/HG002_hs37d5_60x_1.sc.bam
# INS_FASTA=$HOME/StructuralVariantAnnotation/bamsurgeon/paper/seqs_numt_chr1.fa
WD=$HOME/StructuralVariantAnnotation/bamsurgeon/paper/simReads
INS_FASTA=$WD/seqs_rt_chr1.fa

cd $WD
sample_name=chr1_rt_pe_HS25

echo "generating simulated reads"
$ART/art_illumina -ss HS25 -i $INS_FASTA -p -l 150 -f 30 -m 200 -s 10 -o ${sample_name}_

echo "sorting simulated reads"
bwa mem -t 20 $REF ${sample_name}_1.fq ${sample_name}_2.fq | samtools sort -T ${sample_name}_tmp.sorted.bam -@ 20 -o ${sample_name}_sorted.bam -

echo "indexing output bam..."
samtools index -@ 20 ${sample_name}_sorted.bam
