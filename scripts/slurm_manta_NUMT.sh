#!/bin/bash
# this script is now working on SLURM.
# ---------------resource specifications----------------

#SBATCH --job-name=manta_NUMT

#SBATCH --time=40:00:00
#SBATCH --mail-type=ALL

#SBATCH --mail-user=dong.rn@wehi.edu.au

#SBATCH --mem=200G
#SBATCH --cpus-per-task=8
# ---------------end of specifications------------------
module unload python
module load python/2.7.15
module load samtools
HOME=/home/users/allstaff/dong.rn
source $HOME/.bash_profile
REF=$HOME/StructuralVariantAnnotation/bamsurgeon/hs37d5.fa
DIR=$HOME/Papenfuss_lab/projects/StructuralVariantAnnotation/bamsurgeon/paper/simReads
DATA_DIR=$HOME/Papenfuss_lab/projects/StructuralVariantAnnotation/bamsurgeon/paper/simReads
# BAM_NORMAL=$DATA_DIR/COLO829BL_GSC.sorted.bam
# BAM=$DATA_DIR/HG002_simNUMT.sorted.bam
BAM=$DATA_DIR/chr1_numt_pe_HS25_sorted.bam
BAM_NAME=chr1_numt_pe_HS25

# cd $DATA_DIR
# samtools index -@ 32 $BAM &&
# # samtools index -@ 32 $BAM_TUMOR &&
cd $DIR; mkdir $DIR/$BAM_NAME; cd $DIR/$BAM_NAME
[ ! -e /nix ] && exec /stornext/HPCScratch/home/bedo.j/bin/chroot /stornext/HPCScratch/nix $0 "$@"
# /stornext/HPCScratch/home/bedo.j/bin/chroot /stornext/HPCScratch/nix /nix/store/sgm6rlhbhisd16vkzh5jxhhjy2hcb7pa-manta-1.6.0/bin/configManta.py \
/nix/store/sgm6rlhbhisd16vkzh5jxhhjy2hcb7pa-manta-1.6.0/bin/configManta.py \
    --bam $BAM \
    --referenceFasta $REF \
    --runDir $DIR/$BAM_NAME && \
cd $DIR/$BAM_NAME
$DIR/$BAM_NAME/runWorkflow.py -j 32 -g 32 --quiet #&& \
# $SCRIPT_DIR/manta_combine.sh $DIR/results > $SCRIPT_DIR/hgsvc/$BAM_NAME.vcf
echo Done!
