---
title: "gnomadSV"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(plyranges)
library(dplyr)
library(ggplot2)
library(karyoploteR) 
library(svaRetro)
library(TxDb.Hsapiens.UCSC.hg19.knownGene)

hg19.genes <- TxDb.Hsapiens.UCSC.hg19.knownGene
GenomeInfoDb::seqlevelsStyle(hg19.genes) <- "NCBI"#GenomeInfoDb::seqlevelsStyle(gnomad.gr)[1]
hg19.genes <- GenomeInfoDb::keepSeqlevels(hg19.genes, seqlevels(hg19.genes)[1:24], pruning.mode = "coarse")
exons <- exons(hg19.genes, columns=c("exon_id", "tx_id", "tx_name","gene_id"))
```

# GnomadSV

Before running this script, gnomadSV VCF (`gnomad_v2.1_sv.sites.vcf.gz`) from 
`https://storage.googleapis.com/gcp-public-data--gnomad/papers/2019-sv/gnomad_v2.1_sv.sites.vcf.gz`
and RepeatMasker from `http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/rmsk.txt.gz` 
need to be downloaded to the same directory of this Rmarkdown file.

# gnomadSV
Function to parse gnomadSV to breakpointGRanges object
```{r}
gnomadSV <- function(path){
  gnomad.vcf <- readVcf(path)
  gnomad.bnd.gr <- rowRanges(gnomad.vcf)
  mcols(gnomad.bnd.gr) <- cbind(mcols(gnomad.bnd.gr), info(gnomad.vcf))
  testthat::expect_equal(sum(end(gnomad.bnd.gr)-start(gnomad.bnd.gr) != 0), 0) #checking all start equals end
  testthat::expect_equal(sum(is.na(gnomad.bnd.gr$END)), 0) #checking no END is na.
  
  #gnomad.bnd.gr <- gnomad.bnd.gr %>% filter(seqnames==CHR2) #removing interchromosomal events
  gnomad.bnd.grs <- GRangesList(inter.chr = gnomad.bnd.gr %>% filter(seqnames!=CHR2),
                                   intra.chr = gnomad.bnd.gr %>% filter(seqnames==CHR2 | is.na(CHR2))) #gnomadSV v2.1 contains many 
  
  #intra_chrs:
  gnomad.bnd.grs$intra.chr <- gnomad.bnd.grs$intra.chr %>% filter(!END-end<0) #removing records where end is greater than start (>600 of these guys)
  gnomad.bnd.grs$intra.chr <- gnomad.bnd.grs$intra.chr %>% mutate(end=start, sourceId=names(.), partner=paste(names(.), 'bp2', sep = "_"))
  gnomad.bnd.grs$intra.chr_bp2 <- gnomad.bnd.grs$intra.chr %>% `end<-`(., value=.$END) %>% `start<-`(., value=.$END) %>% mutate(sourceId=partner, partner=names(.)) %>% `names<-`(., .$sourceId)
  testthat::expect_length(partner(c(gnomad.bnd.grs$intra.chr, gnomad.bnd.grs$intra.chr_bp2)), length(c(gnomad.bnd.grs$intra.chr, gnomad.bnd.grs$intra.chr_bp2))) #testing for unpaired bps
  
  #inter_chrs:
  gnomad.bnd.grs$inter.chr <- gnomad.bnd.grs$inter.chr %>% mutate(end=start, sourceId=names(.), partner=paste(names(.), 'bp2', sep = "_"))
  gnomad.bnd.grs$inter.chr_bp2 <- GRanges(seqnames = gnomad.bnd.grs$inter.chr$CHR2, ranges = IRanges(gnomad.bnd.grs$inter.chr$END, width=1, names=gnomad.bnd.grs$inter.chr$partner)) %>% 
    `mcols<-`(., value=mcols(gnomad.bnd.grs$inter.chr)) %>% mutate(sourceId=names(.), partner=gnomad.bnd.grs$inter.chr$sourceId) 
  testthat::expect_length(partner(c(gnomad.bnd.grs$inter.chr, gnomad.bnd.grs$inter.chr_bp2)), length(c(gnomad.bnd.grs$inter.chr, gnomad.bnd.grs$inter.chr_bp2)))
  
  testthat::expect_length(partner(unlist(gnomad.bnd.grs, use.names = FALSE)), length(unlist(gnomad.bnd.grs)))
  unlist(gnomad.bnd.grs, use.names = FALSE) #use.names=TRUE will append list name as prefix to gr names, thus failing the partner().
}

```

```{r}
#function from SVEnsemble
wkdir <- getwd()
gnomad.bnd.gr <- suppressWarnings(gnomadSV(paste0(wkdir, "/gnomad_v2.1_sv.sites.vcf.gz")))


gnomad.rt <- rtDetect(filter(gnomad.bnd.gr, FILTER=="PASS"), hg19.genes, maxgap = 1000, minscore = 0.4)
```

exon-exon junctions:
```{r}
# genes <- unlist(transcriptsBy(hg19.genes, by="gene"))
# gnomad.txs <- lapply(gnomad.rt, function(x) unique(unlist(x$junctions$txs)))
# genes.junctions <- lapply(gnomad.txs, function(x) genes[genes$tx_name %in% x])

genes.loci <- lapply(gnomad.rt, function(x) 
  if (length(unique(seqnames(x$junctions)))==1){
    as_granges(data.frame(seqnames=seqnames(x$junctions[1]),                            
                          start=min(start(x$junctions)), 
                          width=1))
    })


gnomad.junctions <- unlist(GRangesList(genes.loci[sapply(genes.loci, function(x) !isEmpty(x))]))
seqlevelsStyle(gnomad.junctions) <- "UCSC"
```

insertion sites:
```{r}
gnomad.insSite <- lapply(gnomad.rt, function(x) x$insSite[!x$insSite$gene_symbol %in% unlist(x$junctions$gene_symbol)])
gnomad.insSite <- unique(unlist(GRangesList(gnomad.insSite[sapply(gnomad.insSite, function(x) !isEmpty(x))])))
seqlevelsStyle(gnomad.insSite) <- "UCSC"
gnomad.insSite.pass <- gnomad.insSite %>% filter(FILTER=="PASS")
```

# RT distribution across the reference genomes

```{r}
pdf(paste0(wkdir,"/gnomadRT.pdf"), width = 15, height = 10)
plot.params <- getDefaultPlotParams(plot.type=2)
plot.params$ideogramheight <- 150
kp <- plotKaryotype(genome="hg19", plot.type = 2, plot.params = plot.params)
#kp <- plotKaryotype(chromosomes = c("chr1", "chr2"), plot.type = 2)
kpDataBackground(kp, data.panel = 1)
kpDataBackground(kp, data.panel = 2)
kpPoints(kp, chr=seqnames(gnomad.junctions), x=start(gnomad.junctions), y=0.5, 
         data.panel = 1, pch = 25, cex=0.5)  
kpPoints(kp, chr=seqnames(gnomad.insSite.pass), x=start(gnomad.insSite.pass), y=0.5, 
         data.panel = 2, pch = '.', cex=0.5) 
dev.off()
```

# Insertion site distribution by repClass
```{r}
read.table(url('http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/rmsk.txt.gz'))
library(readr)
rmsk <- read_delim("rmsk.txt", "\t", escape_double = FALSE, col_names = FALSE, trim_ws = TRUE)
colnames(rmsk)<- c('bin','swScore','milliDiv','milliDel','milliIns','chrom','start','end',
                   'genoLeft','strand','repName','repClass','repFamily','repStart','repEnd','repLeft','id')
rmsk.gr<-GRanges(rmsk)
rmsk.gr<-keepStandardChromosomes(rmsk.gr, pruning.mode="tidy")
```

```{r}
n.rt <- gnomad.insSite.pass %>% filter(rtFoundSum==T) %>% length()
gnomad.insSite.pass.rmsk <- gnomad.insSite.pass %>% filter(rtFoundSum==T) %>% 
  find_overlaps(., rmsk.gr, maxgap = 100) %>% 
  unique() %>% 
  as_tibble() %>%
  dplyr::count(repClass) %>% 
  bind_rows(tibble(repClass='non-repeats', n=n.rt-sum(.$n)))
```

```{r}
gnomad.insSite.pass.rmsk |>
  write_csv('repClass.csv')

gnomad.insSite.pass.rmsk %>% 
  ggplot(., aes(x = reorder(repClass, n), y = n)) + 
  geom_bar(stat = "identity", fill="lightblue3") + coord_flip() + 
  labs(y="count", x="repClass") + ylim(0, 300) +
  geom_text(aes(label=n), hjust=-0.5, color="black",
            position = position_dodge(0.9), size=3.5) +
  #scale_fill_brewer(palette="Accent") + 
  theme_minimal()

ggsave(filename = paste0(wkdir,"/repClass.pdf"), width = 9, height = 6)
```

## Session Information

```{r}
sessionInfo()
```
