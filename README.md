# CODECHECK: svaRetro and svaNUMT

[CODECHECK](https://codecheck.org.uk/) for "[svaRetro and svaNUMT: Modular packages for annotation of retrotransposed transcripts and nuclear integration of mitochondrial DNA in genome sequencing data](https://doi.org/10.1101/2021.08.18.456578)"

Content of `scripts` is from [1].

## References

[1] Dong, Ruining, Cameron, Daniel, Bedo, Justin, & Papenfuss, Anthony T. (2022). Data and scripts for the manuscript of svaRetro and svaNUMT: modular packages for annotating retrotransposed transcripts and nuclear integration of mitochondrial DNA in genome sequencing data [Data set]. Zenodo. https://doi.org/10.5281/zenodo.7053649