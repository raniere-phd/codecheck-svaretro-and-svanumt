.PHONY: codecheck download

codecheck:
	$(MAKE) -C $@

download: simReads vsDinumt vsGRIPper scripts/gnomad_v2.1_sv.sites.vcf.gz scripts/rmsk.txt.gz

simReads.zip:
	wget https://zenodo.org/record/7053649/files/simReads.zip?download=1 -O $@

simReads: simReads.zip
	unzip $< "$@/*"

vsDinumt.zip:
	wget https://zenodo.org/record/7053649/files/vsDinumt.zip?download=1 -O $@

vsDinumt: vsDinumt.zip
	unzip $< "$@/*"

vsGRIPper.zip:
	wget https://zenodo.org/record/7053649/files/vsGRIPper.zip?download=1 -O $@

vsGRIPper: vsGRIPper.zip
	unzip $< "$@/*"

scripts/gnomad_v2.1_sv.sites.vcf.gz:
	wget https://storage.googleapis.com/gcp-public-data--gnomad/papers/2019-sv/gnomad_v2.1_sv.sites.vcf.gz -O $@

scripts/rmsk.txt.gz:
	wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/rmsk.txt.gz -O $@
